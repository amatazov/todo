package com.amatazov.justdoit.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.amatazov.justdoit.R
import com.amatazov.justdoit.databinding.FragmentPageBinding
import com.amatazov.justdoit.presenter.adapters.TasksAdapter
import com.amatazov.justdoit.presenter.decorators.ItemTaskDecorator
import com.amatazov.justdoit.presenter.vms.PageViewModel
import com.amatazov.justdoit.utils.ActionType
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class PageFragment : Fragment() {

    private lateinit var binding: FragmentPageBinding

    private lateinit var textView: TextView
    private lateinit var rv: RecyclerView
    private lateinit var adapter: TasksAdapter
    private lateinit var fab: FloatingActionButton
    private var pageId: Int = 1

    private val vm: PageViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPageBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        textView = binding.textView
        fab = binding.fab
        fab.setOnClickListener {
            vm.addNewTask()
        }

        setupRv()

        setupArch()
    }

    private fun setupRv() {
        rv = binding.rv
        adapter = TasksAdapter(TasksAdapter.ClickListener { item, action ->
            when (action) {
                ActionType.Check.value -> {
                    vm.handleCheck(item)
                }
                ActionType.Click.value -> {
                    vm.navigateToDetail(item)
                }
            }

        })
        rv.adapter = adapter

        val newsMarginStart =
            requireContext().resources.getDimension(R.dimen.item_task_margin_start)
        val newsMarginEnd = requireContext().resources.getDimension(R.dimen.item_task_margin_end)
        val newsMarginBottom =
            requireContext().resources.getDimension(R.dimen.item_task_margin_bottom)
        val newsMarginTop = requireContext().resources.getDimension(R.dimen.item_task_margin_top)

        rv.addItemDecoration(
            ItemTaskDecorator(
                marginTop = newsMarginTop,
                marginBottom = newsMarginBottom,
                marginLeft = newsMarginStart,
                marginRight = newsMarginEnd
            )
        )
    }

    private fun setupArch() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            vm.tasks.collect() {
                when (it.count()) {
                    0 -> {
                        if (rv.visibility != View.GONE)
                            rv.visibility = View.GONE
                        if (textView.visibility != View.VISIBLE)
                            textView.visibility = View.VISIBLE
                    }
                    else -> {
                        if (rv.visibility != View.VISIBLE)
                            rv.visibility = View.VISIBLE
                        if (textView.visibility != View.GONE)
                            textView.visibility = View.GONE
                        adapter.submitList(it)
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            vm.navigateToTaskDetail.collect {
                findNavController().navigate(
                    PageFragmentDirections.actionPageFragmentToTaskDetailFragment(it.pageId, it.taskId)
                )
            }
        }

        vm.updateArgs(pageId)
    }
}
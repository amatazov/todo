package com.amatazov.justdoit.presenter.adapters

import android.text.SpannableString
import android.text.style.StrikethroughSpan
import androidx.databinding.BindingAdapter
import com.amatazov.justdoit.presenter.models.ItemTask
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textview.MaterialTextView


@BindingAdapter("setContent")
fun MaterialTextView.setContent(item: ItemTask?) {
    item?.let {
        when (it.completed) {
            true -> {
                val string = SpannableString(it.content)
                string.setSpan(StrikethroughSpan(), 0, string.length, 0)
                text = string
            }
            false -> {
                text = it.content
            }
        }
    }
}

@BindingAdapter("setChecked")
fun MaterialCheckBox.setChecked(item: ItemTask?) {
    item?.let {
        if (isChecked != it.completed)
            isChecked = it.completed
    }
}
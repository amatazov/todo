package com.amatazov.justdoit.presenter.vms

import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.amatazov.justdoit.domain.models.TaskDm
import com.amatazov.justdoit.presenter.fragments.TaskDetailFragmentArgs
import com.amatazov.justdoit.domain.interactors.ITaskDetailInteractor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class TaskDetailViewModel : ViewModel(), KoinComponent {

    private val taskInteractor: ITaskDetailInteractor by inject()

    private val _task = MutableStateFlow<TaskDm>(TaskDm())
    val task: StateFlow<TaskDm>
        get() = _task

    fun updateHeader(text:String) {
        if (_task.value.header != text && text.isNotEmpty()) {
            viewModelScope.launch(Dispatchers.IO) {
                _task.value.header = text
                taskInteractor.updateTask(_task.value)
            }
        }
    }

    fun updateDescription(text:String) {
        if (_task.value.description != text) {
            viewModelScope.launch(Dispatchers.IO) {
                _task.value.description = text
                taskInteractor.updateTask(_task.value)
            }
        }
    }

    fun updateDefaultArgs(args: TaskDetailFragmentArgs) {
        if (_task.value.pageId != args.pageId) {
            viewModelScope.launch(Dispatchers.IO) {
                _task.emit(taskInteractor.getTask(args.pageId, args.taskId))
            }
        }
    }
}
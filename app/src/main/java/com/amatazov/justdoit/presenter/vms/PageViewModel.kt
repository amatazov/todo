package com.amatazov.justdoit.presenter.vms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.amatazov.justdoit.domain.interactors.IPageInteractor
import com.amatazov.justdoit.presenter.models.ItemTask
import com.amatazov.justdoit.presenter.models.toUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class PageViewModel : ViewModel(), KoinComponent {

    private val pageIteractor: IPageInteractor by inject()

    private val _tasks = MutableStateFlow<List<ItemTask>>(listOf())
    val tasks: StateFlow<List<ItemTask>>
        get() = _tasks

    private var _pageId = MutableStateFlow<Int?>(null)

    private val _loading = MutableStateFlow<Boolean>(false)
    val loading: StateFlow<Boolean>
        get() = _loading

    private val _toast = MutableSharedFlow<String>(replay = 0)
    val toast: SharedFlow<String>
        get() = _toast

    private val _navigateToTaskDetail = MutableSharedFlow<NavParams>(replay = 0)
    val navigateToTaskDetail: SharedFlow<NavParams>
        get() = _navigateToTaskDetail

    init {
        viewModelScope.launch {
            _pageId.collect { pageId ->
                pageId?.let {
                    fetchTasks(pageId)
                }
            }
        }
    }

    fun addNewTask() {
        _pageId.value?.let {
            viewModelScope.launch {
                _navigateToTaskDetail.emit(NavParams(pageId = it, taskId = 0))
            }
        }
    }

    fun navigateToDetail(item: ItemTask) {
        _pageId.value?.let {
            viewModelScope.launch {
                _navigateToTaskDetail.emit(NavParams(pageId = it, taskId = item.id))
            }
        }
    }

    fun handleCheck(item: ItemTask) {
        viewModelScope.launch {
            pageIteractor.handleCheck(item.id)
        }
    }

    fun updateArgs(pageId: Int) {
        if (_pageId.value != pageId) {
            viewModelScope.launch {
                _pageId.emit(pageId)
            }
        }
    }

    private suspend fun changeState(item: ItemTask) {

    }

    private suspend fun fetchTasks(pageId: Int) {
        pageIteractor.getTasksByPage(pageId)
            .onStart {
                _loading.emit(true)
            }
            .onEach {
                _loading.emit(false)
            }
            .catch { throwable ->
                _toast.emit(throwable.message.toString())
            }
            .flowOn(Dispatchers.IO)
            .collect { items ->
                _tasks.emit(items.map { it.toUiModel() }.toMutableList())
            }
    }

    data class NavParams constructor(
        var pageId: Int = 0,
        var taskId: Int = 0
    )
}
package com.amatazov.justdoit.presenter.models

import com.amatazov.justdoit.domain.models.TaskDm

fun TaskDm.toUiModel(): ItemTask {
    val item = ItemTask()
    item.id = this.id
    item.content = this.header
    item.completed = this.completed
    return item
}
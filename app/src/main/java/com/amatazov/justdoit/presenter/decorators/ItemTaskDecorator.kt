package com.amatazov.justdoit.presenter.decorators

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ItemTaskDecorator constructor(
    var marginTop: Float = 0F,
    var marginBottom: Float = 0F,
    var marginLeft: Float = 0F,
    var marginRight: Float = 0F
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val pos = parent.getChildLayoutPosition(view)

        when (pos == 0) {
            true -> {
                outRect.top = marginTop.toInt()
                outRect.bottom = marginBottom.toInt()
            }
            else -> {
                outRect.bottom = marginBottom.toInt()
            }
        }
        outRect.left = marginLeft.toInt()
        outRect.right = marginRight.toInt()
    }
}
package com.amatazov.justdoit.presenter.vms

import androidx.lifecycle.ViewModel
import com.amatazov.justdoit.data.db.dao.PageDao
import com.amatazov.justdoit.data.db.dao.TaskDao
import com.amatazov.justdoit.data.db.models.PageDb
import com.amatazov.justdoit.data.db.models.TaskDb
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.concurrent.TimeUnit
import kotlin.time.Duration

@KoinApiExtension
class MainViewModel : ViewModel(), KoinComponent {


}
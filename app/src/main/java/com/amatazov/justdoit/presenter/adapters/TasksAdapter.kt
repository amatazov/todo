package com.amatazov.justdoit.presenter.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amatazov.justdoit.databinding.ItemTaskBinding
import com.amatazov.justdoit.presenter.models.ItemTask
import com.amatazov.justdoit.utils.ActionType

open class TasksAdapter(
    private val clickListener: ClickListener
):
    ListAdapter<ItemTask, TasksAdapter.ViewHolder>(DiffCallback()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    class ViewHolder private constructor(val binding: ItemTaskBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemTask, clickListener: ClickListener) {
            binding.item = item
            binding.executePendingBindings()
            binding.checkBox.setOnClickListener {
                clickListener.onClick(item, ActionType.Check.value)
            }
            binding.card.setOnClickListener {
                clickListener.onClick(item, ActionType.Click.value)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemTaskBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<ItemTask>() {
        override fun areItemsTheSame(oldItem: ItemTask, newItem: ItemTask): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ItemTask, newItem: ItemTask): Boolean {
            return oldItem == newItem
        }
    }

    class ClickListener(val listener: (item: ItemTask, action: Int) -> Unit) {
        fun onClick(item: ItemTask, action: Int) = listener(item, action)
    }
}
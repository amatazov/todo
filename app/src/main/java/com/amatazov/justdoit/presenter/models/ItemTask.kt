package com.amatazov.justdoit.presenter.models

data class ItemTask constructor(
    var id: Int = 0,
    var content: String = "",
    var completed: Boolean = false
) {
}
package com.amatazov.justdoit.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.amatazov.justdoit.databinding.FragmentTaskDetailBinding
import com.amatazov.justdoit.presenter.vms.TaskDetailViewModel
import com.amatazov.justdoit.utils.asFlow
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import org.koin.core.component.KoinApiExtension
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
@FlowPreview
@KoinApiExtension
class TaskDetailFragment: Fragment() {

    private lateinit var editHeader: TextInputEditText

    private lateinit var editDescription: TextInputEditText

    private lateinit var binding: FragmentTaskDetailBinding
    private val args: TaskDetailFragmentArgs by navArgs()

    private val vm: TaskDetailViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTaskDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editHeader = binding.editHeader
        editDescription = binding.editDescription

        vm.updateDefaultArgs(args)

        setupArch()
    }

    private fun setupArch() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            editHeader.asFlow().debounce(400).collect {
                vm.updateHeader(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            editDescription.asFlow().debounce(400).collect {
                vm.updateDescription(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            vm.task.collect() {
                editHeader.setText(it.header)
                editDescription.setText(it.description)
            }
        }
    }
}
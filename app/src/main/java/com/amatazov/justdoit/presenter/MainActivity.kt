package com.amatazov.justdoit.presenter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.amatazov.justdoit.R
import com.amatazov.justdoit.databinding.ActivityMainBinding
import com.amatazov.justdoit.presenter.vms.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private val vm: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        setupNavController()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.main_navHostFragment)
        return NavigationUI.navigateUp(navController, appBarConfiguration)
    }

    private fun setupNavController() {

        appBarConfiguration = AppBarConfiguration.Builder(topLevelDestinations).build()

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.main_navHostFragment) as NavHostFragment
        navController = navHostFragment.navController
        navController.navInflater.inflate(R.navigation.main_navigation).apply {
            navController.graph = this
        }

        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            title = when (destination.id) {
                R.id.pageFragment -> {
                    applicationContext.resources.getString(R.string.label_tasks)
                }
                R.id.taskDetailFragment -> {
                    applicationContext.resources.getString(R.string.label_tasks)
                }
                else -> {
                    applicationContext.resources.getString(R.string.label_tasks)
                }
            }
        }

        NavigationUI.setupActionBarWithNavController(
            this,
            navController,
            appBarConfiguration
        )
    }

    companion object {
        val topLevelDestinations =
            setOf(R.id.pageFragment)
    }
}
package com.amatazov.justdoit.utils

sealed class Results<out R> {

    data class Success<out T>(val data: T) : Results<T>()
    data class Error(val exception: Exception) : Results<Nothing>()
    object Loading : Results<Nothing>()
}

val Results<*>.succeeded
    get() = this is Results.Success && data != null

fun <T> Results<T>.successOr(fallback: T): T {
    return (this as? Results.Success<T>)?.data ?: fallback
}
package com.amatazov.justdoit.utils

enum class ItemTasksViewType constructor(var value: Int) {
    Task(0),
    Completed(1)
}

enum class ActionType constructor(var value: Int) {
    Check(0),
    Click(1)
}
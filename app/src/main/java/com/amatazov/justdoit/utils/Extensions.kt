package com.amatazov.justdoit.utils

import android.content.Context
import android.text.Editable
import android.util.DisplayMetrics
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import java.time.*
import kotlin.math.roundToInt

fun Float.dpToPx(context: Context): Int {
    val displayMetrics: DisplayMetrics = context.resources.displayMetrics
    return (this * (displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT.toFloat())).roundToInt()
}

fun Long.toZonedDateTime(): ZonedDateTime {
    val instant = Instant.ofEpochMilli(this)
    return ZonedDateTime.ofInstant(instant, ZoneId.systemDefault())
}

@ExperimentalCoroutinesApi
fun EditText.asFlow() = callbackFlow<String> {
    val afterTextChanged: ((Editable?) -> Unit) = { text ->
        offer(text.toString())
    }
    val textChangedListener = addTextChangedListener(afterTextChanged = afterTextChanged)
    awaitClose {
        removeTextChangedListener(textChangedListener)
    }
}
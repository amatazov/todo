package com.amatazov.justdoit.domain.interactors

import com.amatazov.justdoit.domain.models.TaskDm
import com.amatazov.justdoit.domain.repositories.IPageRepository
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class TaskDetailInteractor : ITaskDetailInteractor, KoinComponent {

    private val pageRepository: IPageRepository by inject()

    override suspend fun getTask(pageId: Int, taskId: Int): TaskDm {
        return if (taskId == 0)
            TaskDm(pageId = pageId)
        else {
            pageRepository.getTaskById(taskId) ?: TaskDm(pageId = pageId)
        }
    }

    override suspend fun updateTask(item: TaskDm) {
        pageRepository.saveTask(item)
    }
}
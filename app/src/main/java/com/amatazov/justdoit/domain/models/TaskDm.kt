package com.amatazov.justdoit.domain.models

import com.amatazov.justdoit.utils.toZonedDateTime
import java.time.ZonedDateTime

data class TaskDm constructor(
    var id: Int = 0,
    var pageId: Int = 0,
    var header: String = "",
    var description: String = "",
    var completed: Boolean = false,
    var createdTime: ZonedDateTime = System.currentTimeMillis().toZonedDateTime()
){

}
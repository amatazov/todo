package com.amatazov.justdoit.domain.interactors

import com.amatazov.justdoit.domain.models.TaskDm

interface ITaskDetailInteractor {

    suspend fun getTask(pageId: Int, taskId: Int): TaskDm

    suspend fun updateTask(item: TaskDm)
}
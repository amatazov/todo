package com.amatazov.justdoit.domain.interactors

import com.amatazov.justdoit.domain.models.TaskDm
import com.amatazov.justdoit.domain.repositories.IPageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class PageInteractor : IPageInteractor, KoinComponent {

    private val pageRepository: IPageRepository by inject()

    override fun getTasksByPage(pageId: Int): Flow<List<TaskDm>> {
        return pageRepository.getTasksByPage(pageId).map { items ->
            items.groupBy { item -> item.completed }.flatMap { group ->
                val sortedItems = mutableListOf<TaskDm>()
                when (group.key) {
                    true -> {
                        sortedItems += group.value.sortedByDescending { it.createdTime }
                    }
                    false -> {
                        sortedItems += group.value.sortedByDescending { it.createdTime }
                    }
                }
                sortedItems
            }
        }
    }

    override suspend fun handleCheck(itemId: Int) {
        val itemDm = pageRepository.getTaskById(itemId)
        itemDm?.let {
            it.completed = !it.completed
            pageRepository.saveTask(it)
        }
    }
}
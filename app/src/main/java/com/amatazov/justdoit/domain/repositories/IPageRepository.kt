package com.amatazov.justdoit.domain.repositories

import com.amatazov.justdoit.domain.models.TaskDm
import kotlinx.coroutines.flow.Flow

interface IPageRepository {

    fun getTasksByPage(pageId: Int): Flow<List<TaskDm>>

    suspend fun getTaskById(itemId: Int): TaskDm?

    suspend fun saveTask(item: TaskDm)
}
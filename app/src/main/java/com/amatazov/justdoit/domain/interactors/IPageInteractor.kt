package com.amatazov.justdoit.domain.interactors

import com.amatazov.justdoit.domain.models.TaskDm
import com.amatazov.justdoit.utils.Results
import kotlinx.coroutines.flow.Flow

interface IPageInteractor {

    fun getTasksByPage(pageId: Int): Flow<List<TaskDm>>

    suspend fun handleCheck(itemId: Int)
}
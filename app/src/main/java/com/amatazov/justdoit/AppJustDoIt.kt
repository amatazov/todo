package com.amatazov.justdoit

import android.app.Application
import androidx.room.Room
import com.amatazov.justdoit.data.db.AppDatabase
import com.amatazov.justdoit.data.repositories.PageRepository
import com.amatazov.justdoit.domain.interactors.PageInteractor
import com.amatazov.justdoit.domain.interactors.TaskDetailInteractor
import com.amatazov.justdoit.domain.repositories.IPageRepository
import com.amatazov.justdoit.domain.interactors.IPageInteractor
import com.amatazov.justdoit.domain.interactors.ITaskDetailInteractor
import com.amatazov.justdoit.presenter.vms.MainViewModel
import com.amatazov.justdoit.presenter.vms.PageViewModel
import com.amatazov.justdoit.presenter.vms.TaskDetailViewModel
import com.amatazov.justdoit.utils.Vars
import com.google.firebase.crashlytics.FirebaseCrashlytics
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

class AppJustDoIt : Application() {

    @KoinApiExtension
    override fun onCreate() {
        super.onCreate()

        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG)

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.DEBUG else Level.NONE)
            androidContext(this@AppJustDoIt)
            modules(appModule)
        }
    }

    @KoinApiExtension
    companion object {

        val appModule = module {

            single {
                Room.databaseBuilder(androidApplication(), AppDatabase::class.java, Vars.DB_NAME)
                    .build()
            }

            single<IPageRepository> { PageRepository() }

            single { get<AppDatabase>().pageDao() }
            single { get<AppDatabase>().taskDao() }

            viewModel { MainViewModel() }
            viewModel { PageViewModel() }
            viewModel { TaskDetailViewModel() }

            factory<IPageInteractor> { PageInteractor() }
            factory<ITaskDetailInteractor> { TaskDetailInteractor() }
        }
    }
}
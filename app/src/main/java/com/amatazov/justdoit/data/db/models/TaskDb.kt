package com.amatazov.justdoit.data.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.amatazov.justdoit.data.db.models.TaskDb.Companion.tableName


@Entity(tableName = tableName)
data class TaskDb constructor(

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,

    @ColumnInfo(name = "page_id")
    var pageId: Int = 0,

    @ColumnInfo(name = "header")
    var header: String = "",

    @ColumnInfo(name = "description")
    var description: String = "",

    @ColumnInfo(name = "completed")
    var completed: Boolean = false,

    @ColumnInfo(name = "created_time")
    var createdTime: Long = System.currentTimeMillis(),
){
    companion object {
        const val tableName = "task"
    }
}
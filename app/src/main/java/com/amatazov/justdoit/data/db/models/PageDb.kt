package com.amatazov.justdoit.data.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.amatazov.justdoit.data.db.models.PageDb.Companion.tableName

@Entity(tableName = tableName)
data class PageDb constructor(

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,

    @ColumnInfo(name = "name")
    var name: String = ""
) {

    companion object {
        const val tableName = "page"
    }
}
package com.amatazov.justdoit.data

import com.amatazov.justdoit.data.db.models.TaskDb
import com.amatazov.justdoit.domain.models.TaskDm
import com.amatazov.justdoit.utils.toZonedDateTime

fun TaskDm.updateDbModel(item: TaskDb) {
    item.pageId = this.pageId
    item.header = this.header
    item.description = this.description
    item.completed = this.completed
}

fun TaskDb.toDmModel(): TaskDm {
    val item = TaskDm()
    item.id = this.id
    item.pageId = this.pageId
    item.header = this.header
    item.description = this.description
    item.completed = this.completed
    item.createdTime = this.createdTime.toZonedDateTime()
    return item
}
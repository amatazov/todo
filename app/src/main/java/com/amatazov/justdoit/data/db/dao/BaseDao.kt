package com.amatazov.justdoit.data.db.dao

import androidx.room.*

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(obj: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(obj: List<T>)

    @Delete
    suspend fun delete(obj: T)

    @Update
    suspend fun update(obj: T)

    @Query("DELETE FROM sqlite_sequence WHERE name = :tableName")
    suspend fun deleteSequence(tableName: String)
}
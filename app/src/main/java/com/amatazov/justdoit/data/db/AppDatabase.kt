package com.amatazov.justdoit.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.amatazov.justdoit.data.db.dao.PageDao
import com.amatazov.justdoit.data.db.dao.TaskDao
import com.amatazov.justdoit.data.db.models.PageDb
import com.amatazov.justdoit.data.db.models.TaskDb

@Database(entities = [PageDb::class, TaskDb::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun pageDao(): PageDao
    abstract fun taskDao(): TaskDao
}
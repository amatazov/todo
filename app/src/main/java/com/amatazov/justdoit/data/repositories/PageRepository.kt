package com.amatazov.justdoit.data.repositories

import com.amatazov.justdoit.data.db.dao.TaskDao
import com.amatazov.justdoit.data.db.models.TaskDb
import com.amatazov.justdoit.data.updateDbModel
import com.amatazov.justdoit.data.toDmModel
import com.amatazov.justdoit.domain.models.TaskDm
import com.amatazov.justdoit.domain.repositories.IPageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class PageRepository : IPageRepository, KoinComponent {

    private val taskDao: TaskDao by inject()

    override fun getTasksByPage(pageId: Int): Flow<List<TaskDm>> {
        return taskDao.getPageTasks(pageId).map { items -> items.map { it.toDmModel() } }
    }

    override suspend fun getTaskById(itemId: Int): TaskDm? {
        val item = taskDao.getTask(itemId)
        return item?.toDmModel()
    }

    override suspend fun saveTask(item: TaskDm) {
        if (item.id == 0) {
            val itemDb = TaskDb()
            item.updateDbModel(itemDb)
            val id = taskDao.insert(itemDb)
            item.id = id.toInt()
        } else {
            val itemDb = taskDao.getTask(item.id)
            itemDb?.let {
                item.updateDbModel(itemDb)
                taskDao.update(itemDb)
            }
        }
    }
}
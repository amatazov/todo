package com.amatazov.justdoit.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.amatazov.justdoit.data.db.models.PageDb
import com.amatazov.justdoit.data.db.models.TaskDb
import kotlinx.coroutines.flow.Flow

@Dao
abstract class PageDao: BaseDao<PageDb> {

    @Query("DELETE FROM page")
    abstract suspend fun clearTable()
}
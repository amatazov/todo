package com.amatazov.justdoit.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.amatazov.justdoit.data.db.models.TaskDb
import kotlinx.coroutines.flow.Flow

@Dao
abstract class TaskDao : BaseDao<TaskDb> {

    @Query("SELECT * FROM task WHERE page_id = :pageId")
    abstract fun getPageTasks(pageId: Int): Flow<List<TaskDb>>

    @Query("SELECT * FROM task WHERE id = :itemId LIMIT 1")
    abstract suspend fun getTask(itemId: Int): TaskDb?

    @Query("DELETE FROM task")
    abstract suspend fun clearTable()
}